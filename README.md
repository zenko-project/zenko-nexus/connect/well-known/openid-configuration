# Zenko Connect - Well Known OpenId Configuration Endpoint

OpenId Configuration endpoint for the OIDC Server Zenko Connect that give the configuration of the server

## Table of Contents

- [Release](#release)
- [Deploy](#deploy)


## Release

Changelog must be updated before each release using the [git-cliff tool](https://github.com/orhun/git-cliff)


## Deploy

Update the config file:
- [ ] The environment variable in the [k8s service file](k8s/service.yaml)

```sh
kube apply -f k8s/service.yaml
```

