pub mod app;
#[cfg(feature = "database")]
pub mod database;
#[cfg(feature = "http_client")]
pub mod http_client;
pub mod log;

use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;

use self::{app::AppSetting, log::LogSetting};

#[cfg(feature = "database")]
use self::database::DatabaseSetting;
#[cfg(feature = "http_client")]
use self::http_client::HttpClientSetting;

#[derive(Debug, Clone, Deserialize)]
pub struct Setting {
    pub app: AppSetting,

    pub log: LogSetting,

    #[cfg(feature = "database")]
    pub database: DatabaseSetting,

    #[cfg(feature = "http_client")]
    pub http_client: HttpClientSetting,
}

impl Setting {
    pub fn init() -> Result<Self, ConfigError> {
        Config::builder()
            .add_source(File::with_name("config/default"))
            .add_source(Environment::default())
            .build()?
            .try_deserialize()
    }
}
