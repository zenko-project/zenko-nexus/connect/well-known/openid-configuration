use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct AppSetting {
    pub port: u16,

    pub host: String,
}
