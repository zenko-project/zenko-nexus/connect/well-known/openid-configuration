pub mod state;

use std::{
    net::{IpAddr, SocketAddr},
    sync::Arc,
};

use tracing::info;

use crate::{errors::app::AppError, log::init_tracing, routes::create_routes, settings::Setting};

use self::state::AppStateBuilder;

pub struct App {
    config: Setting,
    state_builder: AppStateBuilder,
}

impl App {
    pub fn new() -> Result<App, AppError> {
        let config = match Setting::init() {
            Ok(config) => config,
            Err(_) => return Err(AppError::InvalidConfig),
        };

        Ok(App {
            config,
            state_builder: AppStateBuilder::default(),
        })
    }

    async fn init(&mut self) -> Result<(), AppError> {
        if let Err(_) = init_tracing(self.config.log.clone()) {
            return Err(AppError::FailToInitTracing);
        };

        Ok(())
    }

    pub async fn start(&mut self) -> Result<(), AppError> {
        if let Err(err) = self.init().await {
            return Err(err);
        };

        let state = match self.state_builder.build(self.config.clone()) {
            Ok(state) => Arc::new(state),
            Err(_) => return Err(AppError::FailedToInitState),
        };
        let routes = create_routes(state);

        let ip: IpAddr = match self.config.app.host.clone().parse() {
            Ok(ip) => ip,
            Err(_) => return Err(AppError::InvalidHost),
        };
        let addr = SocketAddr::new(ip, self.config.app.port);

        info!("listening on {}", addr);

        let server = axum::Server::bind(&addr)
            .serve(routes.into_make_service())
            .await;

        if server.is_err() {
            return Err(AppError::Crashed);
        }
        Ok(())
    }
}
